package com.netzme.controller;

import com.netzme.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
@RequestMapping("/api/person")
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping("")
    public DeferredResult getPerson() {
        DeferredResult deferredResult = new DeferredResult();

        personService.fetchPerson().subscribe(
                person -> deferredResult.setResult(person)
        );

        return deferredResult;
    }
}
