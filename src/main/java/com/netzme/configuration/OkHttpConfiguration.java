package com.netzme.configuration;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.concurrent.TimeUnit;

@Configuration
@ConditionalOnClass(OkHttpClient.class)
public class OkHttpConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(OkHttpConfiguration.class);

    @Bean(name = "randomUserHttpClient")
    public OkHttpClient okHttpLocationHttpClient(RandomuserProperty randomuserProperty) {

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(LOGGER::info);

        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.valueOf(randomuserProperty.getLogLevel()));

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(randomuserProperty.getConnectTimeout(), TimeUnit.MILLISECONDS)
                .readTimeout(randomuserProperty.getReadTimeout(), TimeUnit.MILLISECONDS)
                .writeTimeout(randomuserProperty.getWriteTimeout(), TimeUnit.MILLISECONDS);
        builder.addInterceptor(
                chain -> chain.proceed(chain.request().newBuilder()
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .build())
        );

        return builder.build();
    }
}
