package com.netzme.configuration;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netzme.service.RandomUserEndpointService;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Configuration
@ConditionalOnClass(Retrofit.class)
public class RetrofitConfiguration {

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(JsonParser.Feature.ALLOW_NUMERIC_LEADING_ZEROS, true);
        return objectMapper;
    }

    private Retrofit getRetrofit(OkHttpClient okHttpOrderClient, String host) {
        Retrofit.Builder builder = new Retrofit.Builder();
        if (okHttpOrderClient != null) {
            builder.client(okHttpOrderClient);
        }
        builder.baseUrl(host);
        builder.addConverterFactory(JacksonConverterFactory.create(getObjectMapper()));
        return builder.build();
    }

    @Bean(name = "retrofitRandomUser")
    public Retrofit retrofitRandomUser(RandomuserProperty emailProperty, @Qualifier(value = "randomUserHttpClient") OkHttpClient okHttpOrderClient) {
        return getRetrofit(okHttpOrderClient, emailProperty.getHost());
    }

    @Bean(name = "randomUserEndpointService")
    public RandomUserEndpointService randomUserEndpointService(@Qualifier(value = "retrofitRandomUser") Retrofit retrofitRandomUser) {
        return retrofitRandomUser.create(RandomUserEndpointService.class);
    }
}
