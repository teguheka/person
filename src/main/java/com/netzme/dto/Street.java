package com.netzme.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Street implements Serializable {
    private static final long serialVersionUID = 964158585095431979L;

    private Long number;
    private String name;
}
