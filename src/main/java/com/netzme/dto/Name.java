package com.netzme.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Name implements Serializable {

    private static final long serialVersionUID = 6745415975325166024L;

    private String title;
    private String first;
    private String last;
}
