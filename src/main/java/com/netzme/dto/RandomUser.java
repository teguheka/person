package com.netzme.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RandomUser implements Serializable {

    private static final long serialVersionUID = -6517362256388108909L;

    private String gender;
    private Name name;
    private Location location;
    private Picture picture;
}
