package com.netzme.service;

import com.netzme.common.BaseResponse;
import com.netzme.dto.RandomUser;
import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

public interface RandomUserEndpointService {

    @GET("/api")
    Call<BaseResponse<List<RandomUser>>> callRandomUser();

}
