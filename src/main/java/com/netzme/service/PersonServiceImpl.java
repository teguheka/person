package com.netzme.service;

import com.netzme.common.BaseResponse;
import com.netzme.dto.Person;
import com.netzme.dto.RandomUser;
import io.reactivex.Single;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.util.List;
import java.util.Objects;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    @Qualifier("randomUserEndpointService")
    private RandomUserEndpointService randomUserEndpointService;

    @Override
    public Single<Person> fetchPerson() {
        return Single.create(singleEmitter -> {
            try {
                //call random user
                Response<BaseResponse<List<RandomUser>>> randomUserResponse = randomUserEndpointService.callRandomUser().execute();

                //compose user from response API
                BaseResponse<List<RandomUser>> success = randomUserResponse.body();
                Person person = composePerson(success.getResults());

                //return object when success
                singleEmitter.onSuccess(person);
            } catch (Exception e) {
                singleEmitter.onError(e);
            }
        });
    }

    private Person composePerson(List<RandomUser> randomUserList) {
        if (null != randomUserList && randomUserList.size() > 0) {
            Person person = new Person();
            for (RandomUser randomUser : randomUserList) {
                person.setGender(StringUtils.defaultString(randomUser.getGender()));

                if (Objects.nonNull(randomUser.getPicture())) {
                    person.setPicture(StringUtils.defaultString(randomUser.getPicture().getLarge()));
                }

                if (Objects.nonNull(randomUser.getName())) {
                    String fullname = buildFullnamePerson(randomUser);
                    person.setFullname(StringUtils.defaultString(fullname));
                }

                if (Objects.nonNull(randomUser.getLocation())) {
                    String address = buildAddressPerson(randomUser);
                    person.setAddress(StringUtils.defaultString(address));
                }
            }
            return person;
        }
        return null;
    }

    private String buildFullnamePerson(RandomUser randomUser) {
        StringBuilder sb = new StringBuilder();
        sb.append(randomUser.getName().getTitle());
        sb.append(" ");
        sb.append(randomUser.getName().getFirst());
        sb.append(" ");
        sb.append(randomUser.getName().getLast());
        return sb.toString();
    }

    private String buildAddressPerson(RandomUser randomUser) {
        StringBuilder sb = new StringBuilder();
        sb.append(randomUser.getLocation().getStreet().getName());
        sb.append(" ");
        sb.append(randomUser.getLocation().getCity());
        return sb.toString();
    }

}
