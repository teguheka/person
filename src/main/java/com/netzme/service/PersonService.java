package com.netzme.service;

import com.netzme.dto.Person;
import io.reactivex.Single;

public interface PersonService {

    Single<Person> fetchPerson();
}
