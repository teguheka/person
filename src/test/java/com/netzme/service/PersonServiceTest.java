package com.netzme.service;

import com.netzme.common.BaseResponse;
import com.netzme.dto.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PersonServiceTest {

    @Mock
    private RandomUserEndpointService randomUserEndpointService;

    @Mock
    private Call<BaseResponse<List<RandomUser>>> callGetRandomUser;

    @InjectMocks
    private PersonServiceImpl personService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void fetchPersonTest() throws IOException {
        // mock picture object
        Picture picture = Picture.builder().large("xxx.jpg").build();

        // mock name object
        Name name = Name.builder().title("mr").first("teguh").last("putra").build();

        // mock location object
        Location location = Location.builder()
                .street(new Street(123L, "wayahe"))
                .city("Bekasi")
                .build();


        BaseResponse<List<RandomUser>> response = new BaseResponse<>();
        response.setResults(Collections.singletonList(RandomUser.builder()
                .gender("Male")
                .name(name)
                .location(location)
                .picture(picture)
                .build())
        );

        when(randomUserEndpointService.callRandomUser()).thenReturn(callGetRandomUser);
        when(callGetRandomUser.execute()).thenReturn(Response.success(response));

        Person person = personService.fetchPerson().blockingGet();
        Assert.assertNotNull(person);

        verify(randomUserEndpointService).callRandomUser();
        verify(callGetRandomUser).execute();
    }

    @Test
    public void fetchPersonWithNullLocationTest() throws IOException {
        // mock name object
        Name name = Name.builder().title("mr").first("teguh").last("putra").build();

        // mock picture object
        Picture picture = Picture.builder().large("xxx.jpg").build();

        BaseResponse<List<RandomUser>> responseWithNullLocation = new BaseResponse<>();
        responseWithNullLocation.setResults(Collections.singletonList(RandomUser.builder()
                .gender("Male")
                .picture(picture)
                .name(name)
                .build())
        );

        when(randomUserEndpointService.callRandomUser()).thenReturn(callGetRandomUser);
        when(callGetRandomUser.execute()).thenReturn(Response.success(responseWithNullLocation));

        Person person = personService.fetchPerson().blockingGet();
        Assert.assertNull(person.getAddress());

        verify(randomUserEndpointService).callRandomUser();
        verify(callGetRandomUser).execute();
    }

    @Test
    public void fetchPersonWithNullPictureTest() throws IOException {
        // mock name object
        Name name = Name.builder().title("mr").first("teguh").last("putra").build();

        Location location = Location.builder()
                .street(new Street(123L, "wayahe"))
                .city("Bekasi")
                .build();

        BaseResponse<List<RandomUser>> responseWithNullPicture = new BaseResponse<>();
        responseWithNullPicture.setResults(Collections.singletonList(
                RandomUser.builder()
                        .gender("Male")
                        .name(name)
                        .location(location)
                        .build()
                )
        );

        when(randomUserEndpointService.callRandomUser()).thenReturn(callGetRandomUser);
        when(callGetRandomUser.execute()).thenReturn(Response.success(responseWithNullPicture));

        Person person = personService.fetchPerson().blockingGet();
        Assert.assertNull(person.getPicture());

        verify(randomUserEndpointService).callRandomUser();
        verify(callGetRandomUser).execute();
    }

    @Test
    public void fetchPersonWithNullNameTest() throws IOException {

        // mock picture object
        Picture picture = Picture.builder().large("xxx.jpg").build();

        // mock location object
        Location location = Location.builder()
                .street(new Street(123L, "wayahe"))
                .city("Bekasi")
                .build();


        BaseResponse<List<RandomUser>> responseWithNullName = new BaseResponse<>();
        responseWithNullName
                .setResults(Collections.singletonList(RandomUser.builder()
                        .gender("Male")
                        .location(location)
                        .picture(picture)
                        .build()));

        when(randomUserEndpointService.callRandomUser()).thenReturn(callGetRandomUser);
        when(callGetRandomUser.execute()).thenReturn(Response.success(responseWithNullName));

        Person person = personService.fetchPerson().blockingGet();
        Assert.assertNull(person.getFullname());

        verify(randomUserEndpointService).callRandomUser();
        verify(callGetRandomUser).execute();
    }
}
