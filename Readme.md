# Getting Started


### Stack Technology

* Java 1.8
* Spring Boot version 1.5.21
* Retrofit version 2.3.0
* RxJava version 2.1.7
* Maven version 3
* Docker (optional)

---


### Steps running application without docker

**1. Running using maven, please install maven first before run the apps**
```bash
mvn spring-boot:run
```

**2. Open below URL in your browser**
```
http://localhost:8080/api/person
```
**3. Stop the application**

Pressing `ctrl + c` or `command + c` for stopping the application

---


### Steps running application with docker
**1. Build application using maven by typing the following command**

```bash
mvn clean package
```
**2. Build the docker image by typing the following command**

```bash
docker build -t person-apps
```
**3. Running the Docker Image**
```bash
docker run -p 8080:8080 person-apps
```
**4. Open below URL in your browser**
```
http://localhost:8080/api/person
```
**5. Stop the application**

Pressing `ctrl + c` or `command + c` for stopping the application

---

### Additional note for running apps with Docker
* If you've build the images before, you can direct to step 3
* Delete image ```docker rmi <image-id/name>```
* Running detached mode ```docker run -d -p 8080:8080 person-apps```